﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour
{
    int max;
    int min;
    int guess;

    // Start is called before the first frame update
    void Start()
    {
        IntializeStuff();
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
    }

    private void GetInput()//Check if the number is correct

    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            min = guess;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            max = guess;
            NextGuess();
        }
        else if (Input.GetKeyDown(KeyCode.Return))
        {
            Debug.Log("You got it: \n\n");
            IntializeStuff();
        }
    }

    private void NextGuess()
    {
        guess = (max + min) / 2;
        Debug.Log("Is it higher or lower than: " + guess);
    }


    private void IntializeStuff()
    {
        max = 1000;
        min = 1;
        guess = Random.Range(min, max);
        PrintWelcome();
        max = max + 1;
    }

    void PrintWelcome()
    {
        Debug.Log("Hi All, time to play number wizard: ");
        Debug.Log("Pick a number: ");
        Debug.Log("The highest number is " + max);
        Debug.Log("The lowest number is " + min);
        Debug.Log("Is it higher or lower than: " + guess);
        Debug.Log("Push Up arrow for Higher, Down Arrow for Lower, and Enter for Correct");
    }
}
